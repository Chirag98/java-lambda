package Lampda01_02;

import java.util.Arrays;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.*;

public class Lambda01_02 {
    public static void main(String[] args) {

        Runnable r1 = new Runnable() {
            @Override
            public void run() {
                System.out.println("Runnabake 1");
            }
        };
        Runnable r2 = () -> System.out.println("runnable 2");

        r1.run();
        r2.run();

        BiFunction<Integer,Integer,Integer> sum=(a,b)->(a+b);
        int total=sum.apply(2,5);
        System.out.println(total);
        }


    }




