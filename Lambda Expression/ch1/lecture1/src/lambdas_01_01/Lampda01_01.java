package lambdas_01_01;
import java.util.Date;
import java.util.Locale;
import java.util.function.*;

class Lambdas1 {
    public static void main(String[] args) {

        Predicate<Integer> p= (i)->( i%2==0);
        System.out.println(p.test(15));
        System.out.println(p.test(12));

        Consumer<String> str= (s) -> System.out.println(s.length());
        str.accept("anibdibaibsibsdubesdu");


        Function<Integer,Integer> f1 = (num)->(num*num);
        Function<Integer,String> f2 = (num)->Integer.toString(num);
        System.out.println(f1.andThen(f2).apply(1234));


        Supplier<Date> s  = ()-> new Date();
        System.out.println(s.get());


        BinaryOperator<Integer> add = (a, b) -> a*b;
        System.out.println(add.apply(10, 25));

        UnaryOperator<String> unary  = (msg)-> msg.toLowerCase();
        System.out.println(unary.apply("wertyf"));
    }

}

