package Lampda01_03;

import java.math.BigInteger;
import java.util.function.*;

public class lampda01_03 {
    public static void main(String[] args) {

        // Function is Lambda Expression

        IntFunction<String> intToString = num -> Integer.toString(num);
        System.out.println("length is: " + intToString.apply(123).length());

        Supplier<String> s=()->{
            String otp="";
            for(int i=0;i<6;i++){
                otp=otp+(int)(Math.random()*10);
            }
            return otp;
        };
        System.out.println(s.get());

        Function<String, BigInteger> newBigInt = BigInteger::new; // Consturctor
        System.out.println(" actual value: " + newBigInt.apply("123456789"));

        BinaryOperator<String> f=(s1,s2)-> (s1+s2);
        System.out.println(f.apply("APoorva", "Srivastava"));


        UnaryOperator<Integer> f1= i->i*i;
        System.out.println(f1.apply(6));
    }
}
