package Lambda02_01;

public class bookDetail {
    private String firstName;
    private String lastName;
    private int num;

    public bookDetail(String firstName,String lastName,int num) {
       this.firstName=firstName;
       this.lastName=lastName;
       this.num=num;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName ) {
        this.firstName=firstName;
    }

    public String getlastName() {
        return lastName;
    }

    public void getLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getNum(){
        return num;
    }

    public void setNum(int num){
        this.num=num;
    }


    public String toString()
    {
        return getFirstName()+" and "+getlastName();
    }
}
