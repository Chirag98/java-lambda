package Lambda02_01;

import java.util.*;
import java.util.stream.Collectors;

public class Lambda02_01 {
   public static void main(String[] args) {
       List<String> list = Arrays.asList("ppple", "babana", "ave", "owl");

       Collections.sort(list, new Comparator<String>() {// using Comparator
           @Override
           public int compare(String o1, String o2) {
               return o1.compareTo(o2);
           }
       });

       Collections.sort(list, (a,b)->a.compareTo(b));// using Lambda
       System.out.println(list);

       bookDetail b1=new bookDetail("Apoorva","Srivastava",44);
       bookDetail b2=new bookDetail("Amal","Srivastava",44);

       List<bookDetail> list1 = Arrays.asList(b1,b2);

       int sum=list1.stream().collect(Collectors.summingInt(bookDetail::getNum));
       System.out.println(sum);

       Set<Integer> set=new HashSet<>(Arrays.asList(2,3,4,2,3,42,5));
       System.out.println(set);


   }

}
