package Lambda02_03;

import java.awt.*;
import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Lambda02_03 {
      public static void main(String[] args){

          Arrays.stream(new int[]{2,-4,8,4,6}).map(n -> (n*n)).sorted().forEach(System.out::println);

          Double[] d={10.0,10.1,10.2,10.3};
          Stream<Double> s1=Stream.of(d);
          s1.forEach(System.out::println);

          Stream.of(1.2,3.5,4.66).mapToInt(Double::intValue).forEach(System.out::println);

      }
}
