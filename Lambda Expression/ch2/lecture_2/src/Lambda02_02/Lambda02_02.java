package Lambda02_02;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Lambda02_02 {
    public static void main(String[] args){

        List<String> list= Arrays.asList("zoo","blue","green","apple");

        list.stream().sorted().findFirst().ifPresent(System.out::println);

        List<String> list2=list.stream().sorted((i1,i2)-> i1.compareTo(i2)).collect(Collectors.toList());
        System.out.println(list2);


        Stream.of(2,3,4,5,6,10,34).filter(i->(i%2==0)).collect(Collectors.toList()); //predicate });

        List<String> list1=Stream.of("apple", "pear", "banana").map(str->str.toUpperCase())
                                     .collect(Collectors.toList());
        System.out.println(list1);
    }
}
